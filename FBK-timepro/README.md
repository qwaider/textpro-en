
# TimePro: Temporal expression recognition and normalization for English

***
Author: Paramita Mirza (FBK) and Anne-Lyse Minard (FBK)

Contact: minard@fbk.eu (Anne-Lyse Minard), qwaider@fbk.eu (Mohammed Qwaider)

version: v1.5
***


    Copyright (C) 2015-2016 Fondazione Bruno Kessler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.




It is trained on the TempEval3 training dataset for time expression recognition and classification. For time expression normalization we have used timenorm library (https://github.com/bethard/timenorm). 
The module adds a new NAF layer to the input file: timeExpressions.
Each time expression is represented by a timex3 element as follow:
```
    <timex3 id="tmx1" type="DATE" value="2005-04-27">
      <!--April 27 , 2005-->
      <span>
        <target id="w8"/>
        <target id="w9"/>
        <target id="w10"/>
        <target id="w11"/>
      </span>
    </timex3>
```

A timex3 element has the following attributes:

- id
- type (DATE, DURATION, SET or TIME)
- value (normalized value following ISO-time)
- functionInDocument (CREATION_TIME or NONE)
- anchorTimeID (id of another timex3 element)
- beginPoint (id of another timex3 element)
- endPoint (id of another timex3 element)



Requirements
------------
    * Java 1.6 or higher
    * rm, cat, sed, awk (they are fundamental UNIX tools)
    * tcsh shell
    

Installing TimePro
------------------

1) Clone the repository.
WARNING: be very careful to don't put the spaces in the path (tcsh shell doesn't like spaces!).
```
git clone git@bitbucket.org:qwaider/textpro-en.git
```

2) TextPro needs a Java™ Virtual Machine on your computer. If you don't have Java™ runtime 
environment (JRE) yet, download and install it from http://java.sun.com/javase/downloads/index.jsp.
Then set the JAVA_HOME environment variable to point to the directory where the Java™ runtime 
environment is installed on your computer.
On csh/tcsh shell type this command:
$> setenv JAVA_HOME <directory where the JDK is installed>

3) In order to install yamcha and TinySVM, from the directory execute:
```
./INSTALL.sh
```


Usage TimePro
-------------

INPUT

TimePro needs a NAF file by STDIN, including terms and constituency layers. The encoding must be UTF8. 


OUTPUT

TimePro produces a NAF file with a new layer: timeExpressions.


EXAMPLES 
```
cat input_file | sh run.sh | cat >output_file
```


Third-party tools and resources (see NOTICE)
-------------------------------

* yamcha (http://chasen.org/~taku/software/yamcha/)
* TinySVM (http://chasen.org/~taku/software/TinySVM/)
* timenorm (https://github.com/paramitamirza/timenorm)
* threetenbp-0.8.1 (http://www.threeten.org/)
* scala-library (http://www.scala-lang.org) 
* jdom-2.0.5 
* kaflib-naf (https://github.com/ixa-ehu/kaflib)
* NAFtoTXP and TXPtoNAF (https://github.com/alminard/NAFtoTXP)
* TimeProNorm and TimeProEmptyTimex (https://github.com/alminard/TimeProNorm)


Reference
---------

Paramita Mirza and Anne-Lyse Minard. HLT-FBK: a complete Temporal Processing system for QA TempEval. In Proceedings of the 9th International Workshop on Semantic Evaluation (SemEval 2015).
