#!/bin/sh

NAFFILE=$1

scratchDir=/tmp

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

cd $DIR

RANDOM=`bash -c 'echo $RANDOM'`

FILETXP=$scratchDir/$RANDOM-TimePro.txp
CHUNKIN=$scratchDir/$RANDOM-TimePro.naf
FILEOUT=$scratchDir/$RANDOM-TimeProOUT.txp
TIMEPRONORMIN=$scratchDir/$RANDOM-TimeProNormIN.txp

cat $NAFFILE > $CHUNKIN

BEGINTIME=`date '+%Y-%m-%dT%H:%M:%S%z'`

#Timex recognition
cat $CHUNKIN | java -cp "lib/jdom-2.0.5.jar:lib/kaflib-naf-1.1.9.jar:lib/NAFtoTXP_v11.jar" eu.fbk.newsreader.naf.NAFtoTXP_v11 $FILETXP chunk+entity timex

tail -n +4 $FILETXP | awk -f resources/english-rules-temporal-markers > $FILEOUT
head -n +4 $FILETXP > $TIMEPRONORMIN

cat $FILEOUT | "$DIR"/tools/yamcha-0.33/usr/local/bin/yamcha -m models/tempeval3_silver-data.model >> $TIMEPRONORMIN

#Timex normalization
cat $TIMEPRONORMIN | java -cp "lib/scala-library.jar:lib/timenorm-0.9.1-SNAPSHOT.jar:lib/threetenbp-0.8.1.jar:lib/TimeProNorm_v2.5.jar" eu.fbk.timePro.TimeProNormApply $FILETXP

rm $FILEOUT
rm $TIMEPRONORMIN

#TXP to NAF + Empty timex detection
cat $CHUNKIN | java -cp "lib/jdom-2.0.5.jar:lib/kaflib-naf-1.1.9.jar:lib/NAFtoTXP_v11.jar" eu.fbk.newsreader.naf.NAFtoTXP_v11 $FILEOUT chunk+morpho+timex+event eval

java -Dfile.encoding=UTF8 -cp "lib/TXPtoNAF_v5.jar:lib/jdom-2.0.5.jar:lib/kaflib-naf-1.1.9.jar" eu.fbk.newsreader.naf.TXPtoNAF_v4 $CHUNKIN $FILETXP "$BEGINTIME" TIMEX3 | java -Dfile.encoding=UTF8 -cp "lib/kaflib-naf-1.1.9.jar:lib/jdom-2.0.5.jar:lib/TimeProEmptyTimex_v2.jar" eu.fbk.timepro.TimeProEmptyTimex $FILEOUT

rm $FILETXP
rm $CHUNKIN
rm $FILEOUT
