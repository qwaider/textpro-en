#! /bin/tcsh -f
cd tools/

# install TinySVM-0.09
echo "installing TinySVM-0.09, please wait ..."
gunzip < ./TinySVM-0.09.tar.gz | tar xf -
cd ./TinySVM-0.09
set TSVM_DIR = $PWD
mkdir ./usr
mkdir ./usr/local
#mv ./src/getopt.h ./src/getopt.h-orig
#if ( -f "../M1/src/getopt.h") then
#cp ../M1/src/getopt.h ./src/
#endif
./configure --prefix=$TSVM_DIR/usr/local/ --enable-shared=no
make
make check
make install
cd -

# install yamcha-0.33
echo "installing yamcha-0.33, please wait ..."
gunzip < ./yamcha-0.33.tar.gz | tar xf -
cd ./yamcha-0.33
set YAMCHA_DIR = $PWD
mkdir ./usr
mkdir ./usr/local
./configure --with-svm-learn=$TSVM_DIR/usr/local/bin/svm_learn --prefix=$YAMCHA_DIR/usr/local/ --enable-shared=no
mv ./Makefile ./Makefile-orig
sed -e 's/ln -s -f/ln -f -s/g' Makefile-orig > Makefile
make
make check
make install
./yamcha-config --libexecdir
cp ./usr/local/libexec/yamcha/Makefile ./
cd -

if ( ! -e "./yamcha-0.33/usr/local/bin/yamcha") then
    echo " ...trying to install a modified version of yamcha-0.33, please wait ..."
    gunzip < ./yamcha-0.33_fbk.tar.gz | tar xf -
    cd ./yamcha-0.33
    set YAMCHA_DIR = $PWD
    mkdir ./usr
    mkdir ./usr/local
    ./configure --with-svm-learn=$TSVM_DIR/usr/local/bin/svm_learn --prefix=$YAMCHA_DIR/usr/local/ --enable-shared=no
    make
    make check
    make install
    ./yamcha-config --libexecdir
    cp ./usr/local/libexec/yamcha/Makefile ./
    cd -
endif
cd ..

